@extends('frontend.layouts.master')
@section('content')

<div class="main">
  <div class="shop_top">
    <div class="container">
        <div class="col-lg-3 col-sm-12">
          @include('frontend.member.sidebar')
        </div>
        <div class="col-lg-9 col-sm-12">
          @include('frontend.layouts.alert')
          <h2 class="sub-header">Confirm Payment</h2>
          @if(count($invoice) == 0)
            <small><i>Tidak ada Invoice yang harus dibayar</i></small>
          @else
          {{ Form::open(array('action' => 'UserMemberController@postPayment', 'class' => 'form-horizontal')) }}
          <div class="form-group">
              <label for="courier" class="col-sm-3 control-label">Invoice Code</label>
              <div class="col-sm-9">
                {{ Form::select('invoice', $invoice, null, array('class' => 'form-control')) }}
              </div>
            </div>
            <div class="form-group">
              <label for="amount" class="col-sm-3 control-label">Amount *</label>
              <div class="col-sm-9"> 
                {{ Form::text('amount', null, array('class' => 'form-control', 'placeholder' => 'Jumlah transfer', 'type' => 'text', 'required' => 'required' )) }}
              </div>
            </div>
            <div class="form-group">
              <label for="message" class="col-sm-3 control-label">Message *</label>
              <div class="col-sm-9">
                {{ Form::textarea('message', null, array('rows' => '3', 'class' => 'form-control', 'placeholder' => 'Isikan detail pembayaran Anda, misal : Sudah ane transfer gan ke bank BCA dari bank XXX a/n Budi Santoso', 'required' => 'required' )) }}
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-offset-3 col-sm-9">
              <small><i>* required</i></small>
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-offset-3 col-sm-9">
                  <button class="btn button-black" type="submit">Confirm</button>
              </div>
            </div>
          {{ Form::close() }}
          @endif
        </div>
    </div>
  </div>
</div>
@stop
