@extends('frontend.layouts.master')
@section('content')
	<div class="main">
    	<div class="shop_top">
			<div class="container">
				
				<div class="col-md-6">
					<div class="login-page">
						<h4 class="title">New Customers</h4>
						<p>Silahkan melakukan pendaftaran terlebih dahulu untuk dapat melakukan order produk pada Linux Geekers</p>
						<div class="button1 pull-right"> 
							<a class="button-black btn" href="{{ URL::to('member/register') }}">Create an Account</a>
					 	</div>
						<div class="clear"></div>
					</div>
				</div>
				<div class="col-md-6">
      				@include('frontend.layouts.alert')
					<!-- @if (Session::has('success'))
						<div class="alert alert-success">
							{{ Session::get('success') }}
						</div>
					@endif
					@if (Session::has('error'))
						<div class="alert alert-danger">
							{{ Session::get('error') }}
							{{ HTML::ul($errors->all()) }}
						</div>
					@endif -->
					<div class="login-title">
	           			<h4 class="title">Registered Customers</h4>
						<div id="loginbox" class="loginbox">
							{{ Form::open(array('action' => 'UserMemberController@postLogin', 'id' => 'login-form')) }}
								<fieldset class="input">
								    <p id="login-form-username">
								    	<label for="modlgn_username">Email</label>
                        				{{ Form::text('email', null, array('class' => 'inputbox', 'type' => 'text', 'size' => '18' )) }}
								    	<!-- <input id="modlgn_username" type="text" name="email" class="inputbox" size="18"> -->
								    </p>
								    <p id="login-form-password">
								    	<label for="modlgn_passwd">Password</label>
                        				{{ Form::password('password', array('class' => 'inputbox', 'type' => 'password', 'size' => '18' )) }}
								    	<!-- <input id="modlgn_passwd" type="password" name="password" class="inputbox" size="18"> -->
								    </p>
								    <div class="remember">
										<p id="login-form-remember">
											<label for="modlgn_remember"><a href="{{ URL::action('UserMemberController@getReset') }}">Forget Your Password ? </a></label>
										</p>
										<input type="submit" name="Submit" class="button" value="Login"><div class="clear"></div>
									</div>
								</fieldset>
							{{ Form::close() }}
							<!-- </form> -->
						</div>
					</div>
					<div class="clear"></div>
				</div>
			</div>
		</div>
	</div>
@stop