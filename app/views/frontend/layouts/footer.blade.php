﻿	<div class="footer">
		<div class="container">
			<div class="row">
				<div class="col-md-2">
					<ul class="footer_box">
						@if($sidebar1->content != null)
						{{ $sidebar1->content }}
						@else
						<h4>Pages</h4>
						@foreach(Page::sidebar() as $link)
						<li><a href="{{ URL::to('page/'.$link->slug) }}">{{ $link->title }}</a></li>
						@endforeach
						@endif

					</ul>
				</div>
				<div class="col-md-4">
					<ul class="footer_box">
						@if($sidebar2->content != null)
						{{ $sidebar2->content }}
						@else
						<h4>Contact us</h4>
						<li><a>Address : {{ $company->address }}</a></li>
						<li><a>Phone : {{ $company->phone }}</a></li>
						<li><a>Email : {{ $company->email }}</a></li>
						<li><a>BBM : {{ $company->bbm }}</a></li>
						@endif
					</ul>
				</div>
				<div class="col-md-3">
					<ul class="footer_box">
						<div class="text-center">
							@if($sidebar3->content != null)
							{{ $sidebar3->content }}
							@else
							<h4>Payment</h4>
							<li><img src="{{ asset('images/mandiri.png') }}"></li>
							<li style="height:15px"><a href="#">137-00-0751607-9</a></li>
							<li style="margin-bottom:10px"><a href="#">Anugerah Chandra Utama</a></li>
							<li><img src="{{ asset('images/bca.png') }}"></li>
							<li style="height:15px"><a href="#">0373323672</a></li>
							<li><a href="#">Anugerah Chandra Utama</a></li>
							@endif
						</div>

					</ul>
				</div>
				<div class="col-md-3">
					<ul class="footer_box">
						@if($sidebar4->content != null)
						{{ $sidebar4->content }}
						@else
						<h4>About</h4>
						<li>
							<a>
							{{ $company->description }}
							</a>
						</li>

						<ul class="social">
							<li class="facebook"><a href="http://www.facebook.com/{{ Company::social()->facebook }}"><span> </span></a></li>
							<li class="twitter"><a href="http://www.facebook.com/{{ Company::social()->twitter }}"><span> </span></a></li>
							<li class="instagram"><a href="https://instagram.com/linuxgeekers"><span> </span></a></li>
						</ul>
						@endif
					</ul>
				</div>
			</div>
			<div class="row footer_bottom">
			    <div class="copy">
					<p>Copyright &copy; 2015 Linux Geekers. All rights reserved</p>
				</div>

   			</div>
		</div> <!-- End of container -->
	</div> <!-- End of footer -->
