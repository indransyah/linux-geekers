@extends('frontend.layouts.master')
@section('content')
<style type="text/css">
.borderless tbody tr td, .borderless thead tr th {
  border: none;
}
</style>

<div class="main">
  <div class="shop_top">
    <div class="container">
      @include('frontend.layouts.alert')

      @if(Cart::count() == 0)
      <h4 class="title">Shopping cart is empty</h4>
      <p class="cart">You have no items in your shopping cart.<br>Click<a href="{{ URL::action('UserProductController@getIndex') }}"> here</a> to continue shopping</p>
      @else
      <table class="table table-condensed">
        <thead>
          <tr class="cart_menu">
            <td class="image">Name</td>
            <td class="description"></td>
            <td class="price">Price</td>
            <td class="size text-center">Size</td>
            <td class="quantity text-center">Quantity</td>
            <td class="total">Sub Total</td>
            <td class="text-center" width="10%">Actions</td>
          </tr>
        </thead>
        <tbody>
          @foreach(Cart::content() as $content)
          {{ Form::open(array('action' => 'UserCartController@postUpdate')) }}
          <tr>
            <td class="cart_product">
              <!-- <a href=""> -->
              <!-- <img src="{{ URL::to('/uploads/products/ubuntu.jpg') }}" alt="" width="70"> -->
              <img src="{{ ($content->product->picture->count() == 0) ? URL::to('images/default-product.jpg') : URL::to($content->product->picture->first()->photo->url()) }}" alt="" width="70">
              <!-- </a> -->
            </td>
            <td class="cart_description">
              <h4><a href="{{ URL::to('product/'.$content->product->slug) }}">{{ $content->name }}</a></h4>
              <p>{{ ucwords($content->product->system) }}</p>
            </td>
            <td class="cart_price">
              <p>{{ Helpers::rupiah($content->price) }}</p>
            </td>
            <td class="cart_size text-center">
              <p>{{ strtoupper($content->options->size) }}</p>
            </td>
            <td class="cart_quantity text-center">
              <div class="cart_quantity_button">
                <input class="cart_quantity_input" type="text" name="{{ $content->rowid }}-qty" value="{{ $content->qty }}" size="1"> <small><i>*</i></small>
              </div>
            </td>
            <td class="cart_total">
              <p class="cart_total_price">{{ Helpers::rupiah($content->subtotal) }}</p>
            </td>
            <td class="cart_delete text-center">
              <a class="cart_quantity_delete" href="{{ URL::action('UserCartController@getRemove', $content->rowid) }}"><img src="{{ asset('assets/images/close_edit.png') }}" alt=""/></a>
            </td>
          </tr>
          @endforeach
          <tr>
            <td colspan="4">
              <small>
                <p><i>* Click UPDATE button to update sub total and total cost if you change the quantity</i></p>
                <p><i>** Shipping cost not included</i></p>
              </small>
            </td>
            <td class="text-center">Total</td>
            <td>{{ Helpers::rupiah(Cart::total()) }} <small><i>**</i></small><br /><br />
              <a class="button-black btn" href="{{ URL::action('UserCartController@getCheckout') }}" style="margin:0;">Check Out</a>
            </td>
            <td class="text-center"><span class="button1"><input type="submit" class="btn" value="Update" style="border-radius:0px;"></input></span></td>
          </tr>

          {{ Form::close() }}
          <!-- <tr>
            <td colspan="3">
            </td>
            <td colspan="3">
              <table class="table table-condensed borderless">
                <tr>
                  <td>Cart Sub Total</td>
                  <td>{{ Helpers::rupiah(Cart::total()) }}</td>
                </tr>
                <tr class="shipping-cost">
                  <td>Shipping Cost</td>
                  <td>*</td>
                </tr>
                <tr>
                  <td>Total</td>
                  <td><span>{{ Helpers::rupiah(Cart::total()) }} **</span></td>
                </tr>
                <tr>
                  <td></td>
                  <td>
                    <span class=""><a class="button-black btn" href="" style="margin:0;">Check Out</a></span>
                  </td>
                </tr>
              </table>
            </td>
            <td>
            </td>
            <td></td>
          </tr> -->
          <!-- <tr stle="border:none;">
            <td stle="border:none;" colspan="3">
            </td>
            <td stle="border:none;" colspan="2">Shiping Cost</td>
            <td>*</td>
            <td></td>
          </tr>
          <tr>
            <td colspan="3">
            </td>
            <td colspan="2">Total</td>
            <td>{{ Helpers::rupiah(Cart::total()) }}</td>
            <td></td>
          </tr> -->
        </tbody>
      </table>
      @endif
    </div>
  </div>
</div>
@stop
