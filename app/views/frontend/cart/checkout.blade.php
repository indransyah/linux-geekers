@extends('frontend.layouts.master')
@section('content')
<style type="text/css">
.borderless tbody tr td, .borderless thead tr th {
  border: none;
}
#new {
  display:{{ (count($recipients) == 1 || Input::old('recipient') == 'new') ? 'show' : 'none'}};
}
</style>

<div class="main">
  <div class="shop_top">
    <div class="container">
      {{ Form::open(array('action' => 'UserCartController@postCheckout', 'class' => 'form-horizontal')) }}
        <div class="row">
          <div class="col-md-6">
            <table class="table table-condensed">
              <thead>
                <tr class="cart_menu">
  
                  <td class="image">Name</td>
                  <td class="description"></td>
                  <td class="price">Price</td>
                  <td class="size text-center">Size</td>
                  <td class="quantity text-center">Qty</td>
                  <td class="total">Sub Total</td>
                </tr>
              </thead>
              <tbody>
                @foreach(Cart::content() as $content)
                <tr>
                  <td class="cart_product">
                    <img src="{{ ($content->product->picture->count() == 0) ? URL::to('images/default-product.jpg') : URL::to($content->product->picture->first()->photo->url()) }}" alt="" width="70">
                  </td>
                  <td class="cart_description">
                    <h4><a href="">{{ $content->name }}</a></h4>
                    <p>{{ ucwords($content->product->system) }}</p>
                  </td>
                  <td class="cart_price">
                    <p>{{ Helpers::rupiah($content->price) }}</p>
                  </td>
                  <td class="cart_size text-center">
                    <p>{{ strtoupper($content->options->size) }}</p>
                  </td>
                  <td class="cart_quantity text-center">
                    <p>{{ $content->qty }}</p>
                  </td>
                  <td class="cart_total">
                    <p class="cart_total_price">{{ Helpers::rupiah($content->subtotal) }}</p>
                  </td>
                </tr>
                @endforeach
                <tr>
                  <td colspan="4"><a href="{{ URL::action('UserCartController@getView') }}">Edit Cart</a></td>
                  <td class="text-center">Total</td>
                  <td>{{ Helpers::rupiah(Cart::total()) }}<br /><br />
                  </td>
                </tr>
              </tbody>
            </table>
            <h3>Order Information</h3>
            <div class="form-group">
              <label for="message" class="col-sm-3 control-label">Message</label>
              <div class="col-sm-9">
                <textarea name="message" class="form-control" rows="3" placeholder="Pesan pemesanan (jika ada)"></textarea>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            @include('frontend.layouts.alert')
            <h3>Shipping Information</h3>
            <div class="form-group">
              <label for="courier" class="col-sm-3 control-label">Courier</label>
              <div class="col-sm-9"> 
                {{ Form::select('courier', array('jne' => 'JNE', 'pos' => 'POS', 'tiki' => 'TIKI', 'wahana' => 'Wahana', 'other' => 'Other'), null, array('class' => 'form-control')) }}
<!--                 <select name="courier" class="form-control">
                  <option value="jne">JNE</option>
                  <option value="pos">POS</option>
                  <option value="tiki">Tiki</option>
                  <option value="wahana">Wahana</option>
                  <option value="other">Other</option>
                </select> -->
              </div>
            </div>
            <div class="form-group">
              <label for="note" class="col-sm-3 control-label">Note</label>
              <div class="col-sm-9">
                {{ Form::textarea('note', null, array('rows' => '3', 'class' => 'form-control', 'placeholder' => 'Catatan pengiriman (jika ada)' )) }}
                <!-- <textarea name="note" class="form-control" rows="3" placeholder="Catatan pengiriman (jika ada)"></textarea> -->
              </div>
            </div>
            <h3>Recipient Information</h3>
            @if(count($recipients)>1)
            <div class="form-group">
              <label for="recipient" class="col-sm-3 control-label">Recipient *</label>
              <div class="col-sm-9" id="control"> 
                {{ Form::select('recipient', $recipients, null, array('class' => 'form-control')) }}
                <small><i>Pilih "New" untuk menambah penerima atau alamat baru</i></small>
              </div>
            </div>
            @endif

            <div id="new">
            <div class="form-group">
              <label for="name" class="col-sm-3 control-label">Name *</label>
              <div class="col-sm-9"> 
                {{ Form::text('name', null, array('class' => 'form-control', 'placeholder' => 'Nama penerima', 'type' => 'text')) }}
                <!-- <input type="text" class="form-control" name="name" placeholder="Nama penerima"> -->
              </div>
            </div>
            <div class="form-group">
              <label for="phone" class="col-sm-3 control-label">Phone *</label>
              <div class="col-sm-9">
                {{ Form::text('phone', null, array('class' => 'form-control', 'placeholder' => 'No telp', 'type' => 'number')) }}
                <!-- <input type="text" class="form-control" name="phone" placeholder="No telp"> -->
              </div>
            </div>
            <div class="form-group">
              <label for="address" class="col-sm-3 control-label">Address *</label>
              <div class="col-sm-9">
                {{ Form::textarea('address', null, array('rows' => '3', 'class' => 'form-control', 'placeholder' => 'Alamat lengkap dengan format : [nama tempat / jalan] [no rumah], RT [RT] RW [RW], [desa / kelurahan], [kecamatan], [kabupaten]')) }}
                <!-- <textarea name="address" class="form-control" rows="3" placeholder="Alamat lengkap"></textarea> -->
              </div>
            </div>
            <div class="form-group">
              <label for="city" class="col-sm-3 control-label">City *</label>
              <div class="col-sm-9">
                {{ Form::text('city', null, array('class' => 'form-control', 'placeholder' => 'Kota', 'type' => 'text')) }}
                <!-- <input type="text" class="form-control" name="city" placeholder="Kota"> -->
              </div>
            </div>
            <div class="form-group">
              <label for="province" class="col-sm-3 control-label">Province *</label>
              <div class="col-sm-9">
                {{ Form::text('province', null, array('class' => 'form-control', 'placeholder' => 'Provinsi', 'type' => 'text')) }}
                <!-- <input type="text" class="form-control" name="province" placeholder="Provinsi"> -->
              </div>
            </div>
            <div class="form-group">
              <label for="zip" class="col-sm-3 control-label">Zip Code *</label>
              <div class="col-sm-9">
                {{ Form::text('zip', null, array('class' => 'form-control', 'placeholder' => 'Kode Pos', 'type' => 'number')) }}
                <!-- <input type="text" class="form-control" name="zip" placeholder="Kode Pos"> -->
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-offset-3 col-sm-9">
              <small><i>* required</i></small>
              </div>
            </div>
            </div>

            <div class="form-group">
              <div class="col-sm-offset-3 col-sm-9">
                <!-- <button type="submit" class="btn btn-default">Order</button> -->
                <span class="button1">
                  <a class="btn button-black" data-toggle="modal" data-target="#confirmModal" data-toggle="tooltip" data-placement="right">Order</a>
                </span>
                <div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"> 
                  <div class="modal-dialog"> 
                    <div class="modal-content"> 
                      <div class="modal-header"> 
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button> 
                        <h4 class="modal-title" id="myModalLabel">Order Confirmation</h4> 
                      </div> 
                      <div class="modal-body"> 
                        Apakah Anda telah memeriksa produk yang akan dipesan dan mengisi data-data yang diperlukan? <br />
                        <small>Pilih tombol "Order" untuk melakukan pemesanan dan pilih tombol "Close" untuk kembali pada halaman Checkout untuk memeriksa produk yang akan dipesan dan memeriksa data-data yang diperlukan.</small>
                      </div> 
                      <div class="modal-footer"> 
                        <button type="submit" class="btn btn-info">Order</button> 
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                      </div> 
                    </div> 
                  </div> 
                </div>
              </div>
            </div>
          </div>
        </div>
      {{ Form::close() }}
    </div>
  </div>
</div>
<script type="text/javascript">
$('select[name=recipient]').change(function () {
    if ($(this).val() == 'new') {
        $('#new').show();
    } else {
        $('#new').hide();
    }
});

</script>

@stop
