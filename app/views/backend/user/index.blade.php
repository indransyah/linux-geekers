@extends('backend.layouts.master')
@section('content')
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        View Users
                        <!-- <small>Control panel</small> -->
                    </h1>
                    <!-- <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Blank page</li>
                    </ol> -->
                </section>
                <!-- Main content -->
                <section class="content">
                    @include('backend.layouts.alert')
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <!-- <div class="box-header">
                                    <h3 class="box-title">View Pages</h3>
                                </div> --><!-- /.box-header -->
                                <div class="box-body table-responsive">
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Name</th>
                                                <!-- <th>Slug</th> -->
                                                <!-- <th>Username</th> -->
                                                <th>Email</th>
                                                <th width="7%">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($users as $user)
                                            <tr>
                                                <td>{{ $user->id }}</td>
                                                <td>{{ $user->name }}</td>
                                                <!-- <td>{{ $user->slug }}</td> -->
                                                <!-- <td></td> -->
                                                <td>{{ $user->email }}</td>
                                                <td>
                                                    <!-- <div class="btn-group"> -->
                                                        <a class="btn btn-sm btn-danger" data-toggle="modal" data-target="#deleteModal-{{ $user->id }}" data-toggle="tooltip" data-placement="right" title="Delete page"><i class="fa fa-fw fa-trash-o"></i></a>
                                                        <div class="modal fade" id="deleteModal-{{ $user->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                        <h4 class="modal-title" id="myModalLabel">DELETE CONFIRMATION</h4>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        @if($user->id == Sentry::getUser()->id)
                                                                        Are you sure to delete your account? You will logout after delete your account!
                                                                        @else
                                                                        Are you sure to delete <strong>{{ $user->name }}</strong> from your database ?
                                                                        @endif
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        {{ Form::open(array('action' => array('AdminUserController@deleteDestroy', $user->id), 'method'=>'DELETE')) }}
                                                                        <button type="submit" class="btn btn-danger">Delete</button>
                                                                        {{ Form::close() }}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <!-- </div> -->
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                        <!-- <tfoot>
                                            <tr>
                                                <th>Rendering engine</th>
                                                <th>Browser</th>
                                                <th>Platform(s)</th>
                                                <th>Engine version</th>
                                                <th>CSS grade</th>
                                            </tr>
                                        </tfoot> -->
                                    </table>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                    </div>

                </section><!-- /.content -->
@stop() 