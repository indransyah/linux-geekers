@extends('backend.layouts.master')
@section('content')
<style type="text/css">
.dl-horizontal dt {
    text-align: left;
}
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Customer Detail - <small><strong>{{ $customer->name }}</strong> | {{ $customer->email }}</small>
    </h1>
</section>

<!-- Main content -->
<section class="content">
    @include('backend.layouts.alert')
    <div class="row">

        <div class="col-md-4">
            <div class="box box-solid">
                <div class="box-header">
                    <i class="fa fa-shopping-cart"></i>
                    <h3 class="box-title">Order Information</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <dl class="dl-horizontal">
                        <dt>Name</dt>
                        <dd>{{ $order->user->name }}</dd>
                        <dt>Email</dt>
                        <dd>{{ $order->user->email }}</dd>
                        <dt>Date</dt>
                        <dd>{{ Helpers::date($order->date) }}</dd>
                        <dt>Total</dt>
                        <dd>
                            <strong>{{ Helpers::rupiah($order->invoice->amount) }}</strong><br />
                            <small>
                                <i>
                                    Extra cost :{{ Helpers::rupiah($order->extra_cost) }}<br />
                                    Shipping Cost : {{ Helpers::rupiah($order->shipment->cost) }}
                                </i>
                            </small>
                        </dd>
                        <dt>Message</dt>
                        <dd>{{ ($order->message) ? $order->message : '-' }}</dd>
                        <br>
                        <dt></dt>
                        <dd>
                            <a href="{{URL::action('AdminOrderController@getSendInvoice', $order->id)}}" class="btn btn-info btn-flat" type="submit"><i class="fa fa-envelope"></i> Send Invoice</a>
                        </dd>
                    </dl>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- ./col -->

        <div class="col-md-4">
            <div class="box box-solid">
                <div class="box-header">
                    <i class="fa fa-truck"></i>
                    <h3 class="box-title">Shipping Information</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <dl class="dl-horizontal">
                        <dt>Courier</dt>
                        <dd>{{ strtoupper($order->shipment->courier) }}</dd>
                        <dt>Recipient Name</dt>
                        <dd>{{ $order->shipment->recipient->name  }}</dd>
                        <dt>Phone</dt>
                        <dd>{{ $order->shipment->recipient->phone  }}</dd>
                        <dt>Address</dt>
                        <dd>{{ $order->shipment->recipient->address  }}</dd>
                        <dt>City</dt>
                        <dd>{{ $order->shipment->recipient->city }}</dd>
                        <dt>Province</dt>
                        <dd>{{ $order->shipment->recipient->province }}</dd>
                        <dt>Message</dt>
                        <dd>{{ ($order->shipment->message) ? $order->shipment->message : '-' }}</dd>

                    </dl>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- ./col -->

        <div class="col-md-4">
            <div class="box box-solid">
                <div class="box-header">
                    <i class="fa fa-usd"></i>
                    <h3 class="box-title">Aditional Cost & Status</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <dl class="dl-horizontal">
                        {{ Form::open(array('action' => array('AdminOrderController@postUpdate', $order->id))) }}
                        <dt>Extra Cost (Rupiah)</dt>
                        <dd>
                            {{ Form::text('extra-cost', $order->extra_cost, array('class' => 'form-control', 'type' => 'text' )) }}
                        </dd>
                        <br>
                        <dt>Shipping Cost (Rupiah)</dt>
                        <dd>
                            {{ Form::text('shipping-cost', $order->shipment->cost, array('class' => 'form-control', 'type' => 'text' )) }}
                        </dd>
                        <br>
                        <dt>Status</dt>
                        <dd>
                            {{ Form::select('status', array('belum dikonfirmasi' => 'Belum Dikonfirmasi', 'telah dikonfirmasi' => 'Telah Dikonfirmasi'), $order->status, array('class' => 'form-control')) }}
                        </dd>
                        <br>
                        <dt></dt>
                        <dd>
                            <button class="btn btn-info btn-flat" type="submit"><i class="fa fa-save"></i> Save</button>
                        </dd>
                        {{ Form::close() }}
                    </dl>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- ./col -->

    </div><!-- /.row -->
    <!-- END TYPOGRAPHY -->
</section><!-- /.content -->

@stop()
