@extends('backend.layouts.master')
@section('content')
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        View Users
                        <!-- <small>Control panel</small> -->
                    </h1>
                    <!-- <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Blank page</li>
                    </ol> -->
                </section>
                <!-- Main content -->
                <section class="content">
                    @include('backend.layouts.alert')
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <!-- <div class="box-header">
                                    <h3 class="box-title">View Pages</h3>
                                </div> --><!-- /.box-header -->
                                <div class="box-body table-responsive">
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Name</th>
                                                <!-- <th>Slug</th> -->
                                                <!-- <th>Username</th> -->
                                                <th>Email</th>
                                                <th width="7%">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($customers as $customer)
                                            <tr>
                                                <td>{{ $customer->id }}</td>
                                                <td>{{ $customer->name }}</td>
                                                <!-- <td>{{ $customer->slug }}</td> -->
                                                <!-- <td></td> -->
                                                <td>{{ $customer->email }}</td>
                                                <td>
                                                  <a href="{{ URL::action('AdminCustomerController@getShow', $customer->id) }}" class="btn btn-sm btn-info btn-flat"><i class="fa fa-fw fa-eye"></i></a>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                    </div>
                </section><!-- /.content -->
@stop()
