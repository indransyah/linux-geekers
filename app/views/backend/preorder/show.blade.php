@extends('backend.layouts.master')
@section('content')
<style type="text/css">
.dl-horizontal dt {
    text-align: left;
}
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Pre Order Detail - <small>Product : <strong>{{ $product->name }}</strong></small>
    </h1>
</section>

<!-- Main content -->
<section class="content">
    @include('backend.layouts.alert')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-solid">
                <div class="box-header">
                    <i class="fa fa-barcode"></i>
                    <h3 class="box-title">Pre Order Information</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <th>Product</th>
                                <th>Size</th>
                                <th>Qty</th>
                                <!-- <th>Qty</th>
                                <th>Size</th>
                                <th>Subtotal</th> -->
                            </tr>
                            @foreach($product->withOrderDetail() as $size)
                            <tr>
                                <td></td>
                                <td>{{ $size->size }}</td>
                                <td>{{ $size->sum }}</td>
                            </tr>
                            @endforeach
                            <tr>
                                <td>Total</td>
                                <td>4</td>
                            </tr>
                        </tbody>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- ./col -->
    </div><!-- /.row -->
    <!-- END TYPOGRAPHY -->
</section><!-- /.content -->

@stop() 