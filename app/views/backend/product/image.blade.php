@extends('backend.layouts.master')
@section('content')
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Add Product's Images
                        <!-- <small>Control panel</small> -->
                    </h1>
                                    <!-- <button class="pull-right btn btn-default" id="sendEmail">Send <i class="fa fa-arrow-circle-right"></i></button> -->
                    
                    <!-- <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Blank page</li>
                    </ol> -->
                </section>
                <!-- Main content -->
                <section class="content">
                    @if (Session::has('success'))
                    <div class="alert alert-success alert-dismissable">
                        <i class="fa fa-check"></i>
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <b>Success!</b> {{ Session::get('success') }}
                    </div>
                    @endif
                    @if (Session::has('error'))
                    <div class="alert alert-danger alert-dismissable">
                        <i class="fa fa-ban"></i>
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <b>Ups!</b> {{ Session::get('error') }}
                        {{ HTML::ul($errors->all()) }}
                    </div>
                    <!-- <div class="callout callout-danger">
                        <h4>Ups! {{ Session::get('error') }}</h4>
                        {{ HTML::ul($errors->all()) }}
                    </div> -->
                    @endif
                    <div class='row'>

                        <div class="col-md-12">
                            <!-- <div class='box'> -->
                                <!-- <div class='box-body pad'> -->
                                <!-- <form id="my-awesome-dropzone" action="/target" class="dropzone"></form> -->
                                    {{ Form::open(array('action' => 'AdminProductController@upload', 'class' => 'dropzone', 'id' =>'my-awesome-dropzone', 'files' => 'true')) }}
                                    <!-- <input type="file" name="file" /> -->
                                    <!-- {{ Form::open(array('action' => 'AdminProductController@store', 'files' => 'true')) }} -->
                                        <!-- <div class="form-group"> -->
                                            <!-- <label>Photo</label> -->
                                            <!-- {{ Form::file('file', array('multiple'=>true)) }} -->
                                        <!-- </div> -->
                                    <!-- <div>
                                            <button class="pull-right btn btn-default" type="submit">Add <i class="fa fa-plus"></i></button>
                                        </div> -->
                                    {{ Form::close() }}
                                <!-- </div> -->
                                        <!-- <div> -->
                                <!-- </div> -->
                            <!-- </div>/.box -->
                        </div>
                    </div><!-- ./row -->
                    <div class='row'>
                        <div class="col-md-12">
                            <div class="box-footer clearfix">
                                    <button class="pull-right btn btn-default" id="sendEmail">Send <i class="fa fa-arrow-circle-right"></i></button>
                                </div>
                        <!-- <button class="pull-right btn btn-flat btn-default" type="submit">Upload <i class="fa fa-upload"></i></button> -->
                        <div>
                    </div>
                    </div>
                </section><!-- /.content -->
                <script type="text/javascript">
                $(function() {
                        // Replace the <textarea id="editor1"> with a CKEditor
                        // instance, using default configuration.
                        CKEDITOR.replace('description');
                        //bootstrap WYSIHTML5 - text editor
                        $(".textarea").wysihtml5();
                    });
                </script>
@stop()


