@if (Session::has('success'))
<div class="alert alert-success alert-dismissable">
	<i class="fa fa-check"></i>
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	{{ Session::get('success') }}
</div>
@endif

@if (Session::has('error'))
<div class="alert alert-danger alert-dismissable">
	<i class="fa fa-ban"></i>
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	{{ Session::get('error') }}
	{{ HTML::ul($errors->all()) }}
</div>
@endif

@if (Session::has('info'))
<div class="alert alert-info alert-dismissable">
	<i class="fa fa-info"></i>
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	{{ Session::get('error') }}
</div>
@endif

@if (Session::has('warning'))
<div class="alert alert-warning alert-dismissable">
	<i class="fa fa-warning"></i>
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	{{ Session::get('error') }}
</div>
@endif
