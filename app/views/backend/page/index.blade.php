@extends('backend.layouts.master')
@section('content')
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        View Pages
                        <!-- <small>Control panel</small> -->
                    </h1>
                    <!-- <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Blank page</li>
                    </ol> -->
                </section>
                <!-- Main content -->
                <section class="content">
                    @include('backend.layouts.alert')
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <!-- <div class="box-header">
                                    <h3 class="box-title">View Pages</h3>
                                </div> --><!-- /.box-header -->
                                <div class="box-body table-responsive">
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Title</th>
                                                <!-- <th>Slug</th> -->
                                                <th>Author</th>
                                                <th>Content</th>
                                                <th width="12%">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($pages as $page)
                                            <tr>
                                                <td>{{ $page->id }}</td>
                                                <td>{{ $page->title }}</td>
                                                <!-- <td>{{ $page->slug }}</td> -->
                                                <td>{{ $page->user->name }}</td>
                                                <td>{{{ Str::limit(strip_tags($page->content), 50) }}}</td>
                                                <td>
                                                    <div class="btn-group">
                                                        <a href="{{ URL::action('AdminPageController@edit', $page->id) }}" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Edit page"><i class="fa fa-fw fa-edit"></i></a>
                                                        <a class="btn btn-sm btn-danger" data-toggle="modal" data-target="#deleteModal-{{ $page->id }}" data-toggle="tooltip" data-placement="right" title="Delete page"><i class="fa fa-fw fa-trash-o"></i></a>
                                                        <div class="modal fade" id="deleteModal-{{ $page->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                        <h4 class="modal-title" id="myModalLabel">DELETE CONFIRMATION</h4>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        Are you sure to delete <strong>{{ $page->title }}</strong> from your database ?
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        {{ Form::open(array('action' => array('AdminPageController@destroy', $page->id), 'method'=>'DELETE')) }}
                                                                        <button type="submit" class="btn btn-danger">Delete</button>
                                                                        {{ Form::close() }}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <a href="{{ ($page->featured == 0) ? URL::action('AdminPageController@link', $page->id) : URL::action('AdminPageController@unlink', $page->id) }}" class="btn btn-sm {{ ($page->featured == 1) ? 'btn-info' : 'btn-warning' }}" data-toggle="tooltip" data-placement="top" title="Link the page as navigation menu"><i class="fa fa-fw {{ ($page->featured == 1) ? 'fa-chain' : 'fa-chain-broken' }}"></i></a>

                                                    </div>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                        <!-- <tfoot>
                                            <tr>
                                                <th>Rendering engine</th>
                                                <th>Browser</th>
                                                <th>Platform(s)</th>
                                                <th>Engine version</th>
                                                <th>CSS grade</th>
                                            </tr>
                                        </tfoot> -->
                                    </table>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                    </div>

                </section><!-- /.content -->
@stop() 