@extends('backend.layouts.master')
@section('content')
        
        <!-- daterange picker -->
        {{ HTML::style('assets/backend/css/daterangepicker/daterangepicker-bs3.css') }}
        <!-- <link href="../../css/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" /> -->
        <!-- iCheck for checkboxes and radio inputs -->
        <!-- {{ HTML::style('assets/backend/css/iCheck/all.css') }} -->
        <!-- <link href="../../css/iCheck/all.css" rel="stylesheet" type="text/css" /> -->
        <!-- Bootstrap Color Picker -->
        <!-- {{ HTML::style('assets/backend/css/colorpicker/bootstrap-colorpicker.min.css') }} -->
        <!-- <link href="../../css/colorpicker/bootstrap-colorpicker.min.css" rel="stylesheet"/> -->
        <!-- Bootstrap time Picker -->
        {{ HTML::style('assets/backend/css/timepicker/bootstrap-timepicker.min.css') }}
        <!-- <link href="../../css/timepicker/bootstrap-timepicker.min.css" rel="stylesheet"/> -->

                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        View Report
                        <!-- <small>Control panel</small> -->
                    </h1>
                </section>
                <!-- Main content -->
                <section class="content">
                    @include('backend.layouts.alert')
                    <div class="row">
                         <div class="col-md-6">
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Select Range</h3>
                                </div>
                                <div class="box-body">
                                    {{ Form::open(array('action' => 'AdminReportController@postReport')) }}
                                    <!-- Date range -->
                                    <!-- <div class="form-group">
                                        <label>Date range:</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input name="reservation" type="text" class="form-control pull-right" id="reservation"/>
                                        </div>
                                    </div> -->
                                    <!-- /.form group -->

                                    <!-- Date and time range -->
                                    <div class="form-group">
                                        <label>Date and time range:</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-clock-o"></i>
                                            </div>
                                            <input name="reservationtime" type="text" class="form-control pull-right" id="reservationtime"/>
                                        </div>
                                    </div>
                                    <!-- /.form group -->

                                    <!-- Date and time range -->
                                    <!-- <div class="form-group">
                                        <label>Date range button:</label>
                                        <div class="input-group">
                                            <button class="btn btn-default pull-right" id="daterange-btn">
                                                <i class="fa fa-calendar"></i> Date range picker
                                                <i class="fa fa-caret-down"></i>
                                            </button>
                                        </div>
                                    </div> -->
                                    <!-- /.form group -->
                                    <div class="form-group">
                                        <div class="input-group">
                                            <button type="submit" class="btn btn-success btn-flat pull-right"><i class="fa fa-print"></i> Print</button>
                                        </div>
                                    </div>
                                    <!-- /.form group -->
                                    {{ Form::close() }}
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div><!-- /.col (right) -->
                    </div>

                </section><!-- /.content -->
        

        <!-- InputMask -->
        {{ HTML::script('assets/backend/js/plugins/input-mask/jquery.inputmask.js') }}
        {{ HTML::script('assets/backend/js/plugins/input-mask/jquery.inputmask.date.extensions.js') }}
        {{ HTML::script('assets/backend/js/plugins/input-mask/jquery.inputmask.extensions.js') }}
        <!-- date-range-picker -->
        {{ HTML::script('assets/backend/js/plugins/daterangepicker/daterangepicker.js') }}
        <!-- bootstrap color picker -->
        {{ HTML::script('assets/backend/js/plugins/colorpicker/bootstrap-colorpicker.min.js') }}
        <!-- bootstrap time picker -->
        {{ HTML::script('assets/backend/js/plugins/timepicker/bootstrap-timepicker.min.js') }}
        <!-- AdminLTE App -->
        <!-- {{ HTML::script('assets/backend/js/AdminLTE/app.js') }} -->
        <!-- AdminLTE for demo purposes -->
        <!-- {{ HTML::script('assets/backend/js/AdminLTE/demo.js') }} -->
        <!-- Page script -->
        <script type="text/javascript">
            $(function() {
                //Datemask dd/mm/yyyy
                $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
                //Datemask2 mm/dd/yyyy
                $("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
                //Money Euro
                $("[data-mask]").inputmask();

                //Date range picker
                $('#reservation').daterangepicker();
                //Date range picker with time picker
                // $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
                $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'YYYY-MM-DD HH:mm:ss'});
                //Date range as a button
                // $('#daterange-btn').daterangepicker(
                //         {
                //             ranges: {
                //                 'Today': [moment(), moment()],
                //                 'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
                //                 'Last 7 Days': [moment().subtract('days', 6), moment()],
                //                 'Last 30 Days': [moment().subtract('days', 29), moment()],
                //                 'This Month': [moment().startOf('month'), moment().endOf('month')],
                //                 'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
                //             },
                //             startDate: moment().subtract('days', 29),
                //             endDate: moment()
                //         },
                // function(start, end) {
                //     $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                // }
                // );

                //iCheck for checkbox and radio inputs
                // $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                //     checkboxClass: 'icheckbox_minimal',
                //     radioClass: 'iradio_minimal'
                // });
                //Red color scheme for iCheck
                // $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
                //     checkboxClass: 'icheckbox_minimal-red',
                //     radioClass: 'iradio_minimal-red'
                // });
                //Flat red color scheme for iCheck
                // $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                //     checkboxClass: 'icheckbox_flat-red',
                //     radioClass: 'iradio_flat-red'
                // });

                //Colorpicker
                // $(".my-colorpicker1").colorpicker();
                //color picker with addon
                // $(".my-colorpicker2").colorpicker();

                //Timepicker
                $(".timepicker").timepicker({
                    showInputs: false
                });
            });
        </script>

@stop() 