@extends('backend.layouts.master')
@section('content')

{{ HTML::style('assets/backend/jQuery-File-Upload/css/jquery.fileupload.css') }}

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Homepage Settings
    </h1>
</section>
<!-- Main content -->
<section class="content">
    @include('backend.layouts.alert')
    <div class='row'>
        <div class='col-md-12'>
            <div class='box'>
                <div class="box-header">
                    <h3 class="box-title">Image Slider</h3>
                </div>
                <div class='box-body pad'>
                    <!-- The fileinput-button span is used to style the file input field as button -->
                    <span class="btn btn-success fileinput-button">
                        <i class="glyphicon glyphicon-plus"></i>
                        <span>Select files...</span>
                        <!-- The file input field used as target for the file upload widget -->
                        <input id="fileupload" type="file" name="files[]" multiple>
                    </span>
                    <br>
                    <br>
                    <!-- The global progress bar -->
                    <div id="progress" class="progress">
                        <div class="progress-bar progress-bar-success"></div>
                    </div>
                    <!-- The container for the uploaded files -->
                    <div id="files" class="files row">
                        @foreach($images as $image)
                        <div id="row{{ $image->id }}" style="margin-bottom: 10px;" class="col-md-3">
                            <img src="{{ $image->carousel->url() }}" style="width:100%;height:200px;">
                            <a class="btn btn-flat btn-danger btn-block" href="{{ URL::action('AdminSettingController@getDelete', $image->id) }}" type="button">DELETE<i class="fa fa-fw fa-trash-o"></i></a>
                        </div>
                        @endforeach
                        <!-- <div id="row55" style="margin-bottom: 10px;" class="col-md-3">
                            <img src="/images/carousels/000/000/055/medium/original2.jpg" style="width:100%;height:200px;">
                            <a class="btn btn-flat btn-danger btn-block" href="http://localhost:8000/adidev/setting/delete/55" type="button">DELETE</a>
                        </div> -->
                    </div>
                </div>
            </div><!-- /.box -->
        </div><!-- /.col-->
    </div><!-- ./row -->

    <div class='row'>
        <div class='col-md-3'>
            <div class='box'>
                <div class="box-header">
                    <h3 class="box-title">Bottom Left Sidebar 1</h3>
                </div>
                <div class='box-body pad'>
                    {{ Form::open(array('action' => array('AdminSettingController@postSidebar', '1'))) }}
                    <div class="form-group">
                        {{ Form::textarea('content', Sidebar::find(1)->content, array('class' => 'ckeditor form-control', 'rows' => '10', 'cols' => '80','required' => 'true' )) }}
                    </div>
                    <small><i>* leave blank for default</i></small>

                    <div class="box-footer clearfix"> 
                        <button class="pull-right btn btn-default" type="submit">Save <i class="fa fa-plus"></i></button> 
                    </div>
                    {{ Form::close() }}
                </div>
            </div><!-- /.box -->
        </div><!-- /.col-->

        <div class='col-md-3'>
            <div class='box'>
                <div class="box-header">
                    <h3 class="box-title">Bottom Left Sidebar 2</h3>
                </div>
                <div class='box-body pad'>
                    {{ Form::open(array('action' => array('AdminSettingController@postSidebar', '2'))) }}
                    <div class="form-group">
                        {{ Form::textarea('content', Sidebar::find(2)->content, array('class' => 'ckeditor form-control', 'rows' => '10', 'cols' => '80','required' => 'true' )) }}
                    </div>
                    <small><i>* leave blank for default</i></small>

                    <div class="box-footer clearfix"> 
                        <button class="pull-right btn btn-default" type="submit">Save <i class="fa fa-plus"></i></button> 
                    </div>
                    {{ Form::close() }}

                </div>
            </div><!-- /.box -->
        </div><!-- /.col-->

        <div class='col-md-3'>
            <div class='box'>
                <div class="box-header">
                    <h3 class="box-title">Bottom Right Sidebar 2</h3>
                </div>
                <div class='box-body pad'>
                    {{ Form::open(array('action' => array('AdminSettingController@postSidebar', '3'))) }}
                    <div class="form-group">
                        {{ Form::textarea('content', Sidebar::find(3)->content, array('class' => 'ckeditor form-control', 'rows' => '10', 'cols' => '80','required' => 'true' )) }}
                    </div>
                    <small><i>* leave blank for default</i></small>

                    <div class="box-footer clearfix"> 
                        <button class="pull-right btn btn-default" type="submit">Save <i class="fa fa-plus"></i></button> 
                    </div>
                    {{ Form::close() }}

                </div>
            </div><!-- /.box -->
        </div><!-- /.col-->

        <div class='col-md-3'>
            <div class='box'>
                <div class="box-header">
                    <h3 class="box-title">Bottom Right Sidebar 1</h3>
                </div>
                <div class='box-body pad'>
                    {{ Form::open(array('action' => array('AdminSettingController@postSidebar', '4'))) }}
                    <div class="form-group">
                        {{ Form::textarea('content', Sidebar::find(4)->content, array('class' => 'ckeditor form-control', 'rows' => '10', 'cols' => '80','required' => 'true' )) }}
                    </div>
                    <small><i>* leave blank for default</i></small>

                    <div class="box-footer clearfix"> 
                        <button class="pull-right btn btn-default" type="submit">Save <i class="fa fa-plus"></i></button> 
                    </div>
                    {{ Form::close() }}
                </div>
            </div><!-- /.box -->
        </div><!-- /.col-->
    </div><!-- ./row -->
</section><!-- /.content -->

{{ HTML::script('assets/backend/jQuery-File-Upload/js/vendor/jquery.ui.widget.js') }}
{{ HTML::script('assets/backend/jQuery-File-Upload/js/jquery.iframe-transport.js') }}
{{ HTML::script('assets/backend/jQuery-File-Upload/js/jquery.fileupload.js') }}
<script>
/*jslint unparam: true */
/*global window, $ */
$(function () {
    'use strict';
    // Change this to the location of your server-side upload handler:
    var url = '{{ URL::action('AdminSettingController@postImage') }}';
    $('#fileupload').fileupload({
        url: url,
        add: function(e, data) {
                var uploadErrors = [];
                // var acceptFileTypes = /^image\/(gif|jpe?g|png)$/i;
                var acceptFileTypes = /(\.|\/)(gif|jpe?g|png)$/i;

                if(data.originalFiles[0]['type'].length && !acceptFileTypes.test(data.originalFiles[0]['type'])) {
                    uploadErrors.push('Not an accepted file type');
                }
                if(data.originalFiles[0]['size'].length && data.originalFiles[0]['size'] > 5000000) {
                    uploadErrors.push('Filesize is too big');
                }
                if(uploadErrors.length > 0) {
                    alert(uploadErrors.join("\n"));
                } else {
                    data.submit();
                }
        },
        dataType: 'json',
        done: function (e, data) {
            $.each(data.result.files, function (index, file) {
                // $('<p/>').text(file.name).appendTo('#files');
                // $('<img/>').attr('class', 'col-md-3').attr('src', file.name).appendTo('#files');

                $('<div/>').attr('class', 'col-md-3').attr('style', 'margin-bottom: 10px;').attr('id', 'row'+data.result.id).appendTo('#files');
                $('<img/>').attr('src', file.name).attr('style', 'width:100%;height:200px;').appendTo('#row'+data.result.id);
                // $('<div/>').attr('class', 'btn-group').attr('id', 'group'+data.result.id).appendTo('#row'+data.result.id);
                $('<a/>').attr('id', 'delete'+data.result.id).attr('type', 'button').attr('href', '{{ URL::action('AdminSettingController@getDelete') }}/'+data.result.id).attr('class', 'btn btn-flat btn-danger btn-block').text('DELETE').appendTo('#row'+data.result.id);
                $('<i/>').attr('class', 'fa fa-fw fa-trash-o').appendTo('#delete'+data.result.id);
                // $('<a/>').attr('type', 'button').attr('href', '{{ URL::action('AdminSettingController@getDelete') }}/'+data.result.id).attr('class', 'btn btn-flat btn-danger btn-block').text('DELETE').appendTo('#group'+data.result.id);
                // $('<a/>').attr('type', 'button').attr('href', '{{ URL::action('AdminSettingController@getDelete') }}/'+data.result.id).attr('class', 'btn btn-flat btn-danger btn-block').text('DELETE').appendTo('#group'+data.result.id);
            });
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .progress-bar').css(
                'width',
                progress + '%'
            );
        }
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
});
</script>
<script type="text/javascript">
$(function() {
    CKEDITOR.config.toolbar = [
    [ 'Source', '-', 'Bold', 'Italic', 'NumberedList', 'BulletedList','Link', 'Unlink', 'Anchor', 'Copy', 'Paste', 'Image', 'Styles', 'Format' ]
    ];
});
</script>
@stop()