@extends('backend.layouts.master')
@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Company Profile
    </h1>
</section>
<!-- Main content -->
<section class="content">
    @include('backend.layouts.alert')
    <div class='row'>
        <div class='col-md-6'>
            <div class='box'>
                <div class='box-body pad'>
                    {{ Form::open(array('action' => 'AdminSettingController@postCompany', 'files' => 'true')) }}
                    <div class="form-group">
                        <label>Company Name</label>
                        {{ Form::text('name', $company->name, array('class' => 'form-control', 'required' => 'true')) }}
                    </div>
                    <div class="form-group">
                        <label>Description</label>
                        {{ Form::textarea('description', $company->description, array('class' => 'form-control', 'rows' => '10', 'cols' => '80','required' => 'true' )) }}
                    </div>
                    <div class="form-group">
                        <label>Address</label>
                        {{ Form::textarea('address', $company->address, array('class' => 'form-control', 'rows' => '10', 'cols' => '80','required' => 'true' )) }}
                    </div>
                    <div class="form-group">
                        <label>Phone</label>
                        {{ Form::text('phone', $company->phone, array('class' => 'form-control', 'required' => 'true' )) }}
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        {{ Form::text('email', $company->email, array('class' => 'form-control', 'required' => 'true' )) }}
                    </div>
                    <div class="form-group">
                        <label>BBM</label>
                        {{ Form::text('bbm', $company->bbm, array('class' => 'form-control', 'required' => 'true' )) }}
                    </div>
                    <div class="form-group">
                        <label>Facebook</label>
                        {{ Form::text('facebook', $company->facebook, array('class' => 'form-control' )) }}
                    </div>
                    <div class="form-group">
                        <label>Twitter</label>
                        {{ Form::text('twitter', $company->twitter, array('class' => 'form-control' )) }}
                    </div>
                    <div class="form-group">
                        <label>Logo</label>
                        <!-- {{ ($company->logo != null) ? 'ada' : 'tidak ada' }} -->
                        {{ Form::file('logo') }} <br>
                        <img src="{{ $company->logo->url() }}" width="25%">
                    </div>
                    <div class="box-footer clearfix">
                        <button class="pull-right btn btn-info btn-flat" type="submit"><i class="fa fa-save"></i> Save</button>
                    </div>
                    {{ Form::close() }}
                </div>
            </div><!-- /.box -->
        </div><!-- /.col-->
    </div><!-- ./row -->
</section><!-- /.content -->
@stop()