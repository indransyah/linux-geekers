<!DOCTYPE html>
<html class="bg-black">
    <head>
        <meta charset="UTF-8">
        <title>Linux Geekers | Log in</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        {{ HTML::style('assets/backend/css/bootstrap.min.css') }}
        {{ HTML::style('assets/backend/font-awesome-4.2.0/css/font-awesome.min.css') }}
        {{ HTML::style('assets/backend/css/AdminLTE.css') }}

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="bg-black">

        <div class="form-box" id="login-box">
                    
            <div class="header">
                <!-- <a href="{{ URL::to('home') }}"><img src="{{ asset('assets/images/lg.png') }}" alt=""/></a> <span style="font-family: 'Kaushan Script', cursive;">LINUX GEEKERS</span> --> 
                <a href="{{ URL::to('home') }}"><img src="{{ Company::find(1)->logo->url('medium') }}" alt=""/></a>
            </div>
            {{ Form::open(array('action' => 'AdminUserController@postLogin')) }}
                <div class="body bg-gray">
                    @if (Session::has('error'))
                        <div class="callout callout-danger">
                            {{ Session::get('error') }}
                            {{ HTML::ul($errors->all()) }}
                        </div>
                    @endif
                    <div class="form-group">
                        {{ Form::text('email', null, array('class' => 'input-lg form-control', 'placeholder' => 'Email', 'required' => 'true' )) }}
                    </div>
                    <div class="form-group">
                        {{ Form::password('password', array('class' => 'input-lg form-control', 'placeholder' => 'Password', 'required' => 'true' )) }}
                    </div>          
                    <!-- <div class="form-group">
                        <input type="checkbox" name="remember_me"/> Remember me
                    </div> -->
                </div>
                <div class="footer">                                                               
                    <button type="submit" class="btn btn-block btn-flat btn-lg btn-info">Login</button>  
                    <!-- <p><a href="#">I forgot my password</a></p> -->
                    <!-- <a href="" class="text-center">Register a new membership</a> -->
                </div>
            {{ Form::close() }}
        </div>

        {{ HTML::script('assets/backend/js/jquery.min.js') }}
        {{ HTML::script('assets/backend/js/bootstrap.min.js') }}

    </body>
</html>