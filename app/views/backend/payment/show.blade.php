@extends('backend.layouts.master')
@section('content')
<style type="text/css">
.dl-horizontal dt {
    text-align: left;
}
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Payment Detail - <small>INVOICE : <strong><a href="{{ URL::action('AdminOrderController@getShow', $payment->invoice->order->id) }}">{{ strtoupper($payment->invoice->code) }}</a></strong></small><span class="pull-right" style="color: {{ ($payment->status == 'lunas') ? 'green' : 'red' }}">{{ strtoupper($payment->status) }}</span>
    </h1>
</section>

<!-- Main content -->
<section class="content">
    @include('backend.layouts.alert')
    <div class="row">
        <div class="col-md-6">
            <div class="box box-solid">
                <div class="box-header">
                    <i class="fa fa-credit-card"></i>
                    <h3 class="box-title">Payment Information</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <dl class="dl-horizontal"> 
                        {{ Form::open(array('action' => array('AdminPaymentController@postUpdate', $payment->id))) }}
                        <dt>Date</dt>
                        <dd>{{ Helpers::date($payment->date) }}</dd>
                        <dt>Amount</dt>
                        <dd>{{ Helpers::rupiah($payment->amount) }}</dd>
                        <dt>Message</dt>
                        <dd>{{ $payment->message }}</dd>
                        <dt>Status</dt>
                        <dd>
                            {{ Form::select('status', array('proses' => 'Proses', 'lunas' => 'Lunas'), $payment->status, array('class' => 'form-control')) }}
                        </dd>
                        <br>
                        <dt></dt>
                        <dd>
                            <button class="btn btn-info btn-flat" type="submit"><i class="fa fa-save"></i> Save</button>
                            <!-- <a href="{{ URL::action('AdminPaymentController@getReset', $payment->id) }}" class="btn btn-danger btn-flat" type="submit"><i class="fa fa-refresh"></i> Reset</a> -->
                        </dd>
                        {{ Form::close() }}
                    </dl>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- ./col -->

        <div class="col-md-6">
            <div class="box box-solid">
                <div class="box-header">
                    <i class="fa fa-shopping-cart"></i>
                    <h3 class="box-title">Order Information</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <dl class="dl-horizontal">
                        <dt>Name</dt>
                        <dd>{{ $payment->invoice->order->user->name }}</dd>
                        <dt>Email</dt>
                        <dd>{{ $payment->invoice->order->user->email }}</dd>
                        <dt>Date</dt>
                        <dd>{{ Helpers::date($payment->invoice->order->date) }}</dd>
                        <dt>Total</dt>
                        <dd>
                            <strong>{{ Helpers::rupiah($payment->invoice->amount) }}</strong><br />
                            <small>
                                <i>
                                    Extra cost :{{ Helpers::rupiah($payment->invoice->order->extra_cost) }}<br />
                                    Shipping Cost : {{ Helpers::rupiah($payment->invoice->order->shipment->cost) }}
                                </i>
                            </small>
                        </dd>
                        <dt>Message</dt>
                        <dd>{{ ($payment->invoice->order->message) ? $payment->invoice->order->message : '-' }}</dd>
                        <br>
                        <dt></dt>
                        <dd>
                            <a href="{{ URL::action('AdminOrderController@getShow', $payment->invoice->order->id) }}">More detail ...</a>
                        </dd>
                    </dl>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- ./col -->
    </div>
</section>

@stop() 