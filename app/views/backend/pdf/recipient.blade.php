<html>
<head>
<title>Invoice</title>
<style type="text/css">
  #page-wrap {
    width: 700px;
    margin: 0 auto;
  }
  .center-justified {
    text-align: justify;
    margin: 0 auto;
    width: 30em;
  }
  table.outline-table {
    border: 1px solid;
    border-spacing: 0;
  }
  tr.border-bottom td, td.border-bottom {
    border-bottom: 1px solid;
  }
  tr.border-top td, td.border-top {
    border-top: 1px solid;
  }
  tr.border-right td, td.border-right {
    border-right: 1px solid;
  }
  tr.border-right td:last-child {
    border-right: 0px;
  }
  tr.center td, td.center {
    text-align: center;
    vertical-align: text-top;
  }
  td.pad-left {
    padding-left: 5px;
  }
  tr.right-center td, td.right-center {
    text-align: right;
    padding-right: 50px;
  }
  tr.right td, td.right {
    text-align: right;
  }
  .grey {
    background:grey;
  }
</style>
</head>
<body>
  <div id="page-wrap">
    <table width="100%">
      <tbody>
        <tr>
          <td width="60%">
            <h1>To : </h1>
            <strong>{{ $recipient->name }}</strong><br>
            <strong>{{ $recipient->address }}</strong><br>
            <strong>{{ $recipient->city }}</strong><br>
            <strong>{{ $recipient->province }}</strong><br>
            <strong>{{ $recipient->zip_code }}</strong><br>
            <strong>{{ $recipient->phone }}</strong><br>
            <br>
          </td>
          <td width="40%">
            <h1>From :</h1>
            <!-- <img src="http://www.linuxgeekers.com/assets/images/logo.jpg"><br> -->
            <strong>Linux Geekers</strong> <br />
            <strong>Address :</strong> Jl.Imogiri Barat Km 12 Bantul Yogyakarta <br />
            <strong>Email : </strong>  order@linuxgeekers.com <br />
            <strong>Phone : </strong>  085729009963 <br />
            <strong>BBM : </strong>  7CB2DABD
          </td>
        </tr>
      </tbody>
    </table>
  </div>
</body>
</html>
