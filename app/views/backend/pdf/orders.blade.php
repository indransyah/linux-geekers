<html>
<head>
<title>Report</title>
<style type="text/css">
  #page-wrap {
    width: 700px;
    margin: 0 auto;
  }
  .center-justified {
    text-align: justify;
    margin: 0 auto;
    width: 30em;
  }
  table.outline-table {
    border: 1px solid;
    border-spacing: 0;
  }
  tr.border-bottom td, td.border-bottom {
    border-bottom: 1px solid;
  }
  tr.border-top td, td.border-top {
    border-top: 1px solid;
  }
  tr.border-right td, td.border-right {
    border-right: 1px solid;
  }
  tr.border-right td:last-child {
    border-right: 0px;
  }
  tr.center td, td.center {
    text-align: center;
    vertical-align: text-top;
  }
  td.pad-left {
    padding-left: 5px;
  }
  tr.right-center td, td.right-center {
    text-align: right;
    padding-right: 50px;
  }
  tr.right td, td.right {
    text-align: right;
  }
  .grey {
    background:grey;
  }
  .no-margin {
    margin: 0px;
  }
</style>
</head>
<body>
  <div id="page-wrap">
    <h1>Order Report</h1>
    <h3 class="no-margin">{{ Helpers::date($range[0]) }} - {{ Helpers::date($range[1]) }}</h3>
    <br>
    <!-- <strong>Orders</strong> -->
    <table width="100%" class="outline-table">
      <tbody>
        <tr class="border-bottom border-right center">
          <td><strong>Invoice</strong></td>
          <td><strong>Date</strong></td>
          <td><strong>Product</strong></td>
          <td><strong>Shipping</strong></td>
          <td><strong>Payment</strong></td>
          <td><strong>Omset</strong></td>
        </tr>
        @foreach($orders as $order)
        <tr class="border-bottom border-right center">
          <td>{{ strtoupper($order->invoice->code) }}</td>
          <td>{{ Helpers::date($order->date) }}</td>
          <td>
            @foreach($order->product as $product)
              {{ $product->name }} <br> ({{ $product->pivot->qty }} x {{ Helpers::rupiah($product->pivot->price) }}) <br>
            @endforeach
              <hr>
              {{ Helpers::rupiah($order->total) }}
          </td>
          <td>{{ Helpers::rupiah($order->shipment->cost) }}</td>
          <td>{{ Helpers::rupiah($order->invoice->amount) }}</td>
          <td>{{ Helpers::rupiah($order->invoice->amount - $order->shipment->cost) }}</td>
        </tr>
        @endforeach
        <tr>
          <td class="border-right center" colspan="5"><strong>Total</strong></td>
          <td class="center"><strong>{{ Helpers::rupiah($orders->sum('total')) }}</strong></td>
        </tr>
      </tbody>
    </table>
    <br>
  </div>
</body>
</html>
