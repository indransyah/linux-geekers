<?php

class UserHomeController extends BaseController {

	protected $layout = 'frontend.layouts.master';

	public function getIndex() {
		// $product = Product::find(3);
		// return $product->size()->where('value', 'm')->first()->pivot->quota;


		// return Product::featured()->get()->count();
		// foreach (Product::featured()->get() as $product) {
		// 	echo $product->name;
		// }
		// return 'ok';


		// return Product::has('order')->selectRaw('count(orders.id) as aggregate')
  //       ->groupBy('pivot_product_id')->get();

		// return Product::with(['order' => function ($q) {
  //     		$q->wherePivot('size','m');
		// }])->get();

		// FIX
		// $products = DB::table('products')
		// 	->join('order_detail','products.id', '=', 'order_detail.product_id')
		// 	->select(DB::raw('products.*, SUM(qty) as sum'))
		// 	->groupBy('products.id')
		// 	->orderBy('sum', 'DESC')
		// 	->get();

		// $products = Product::featured()->get();

		// return $products[0]->picture[0]->photo->url();
		// return DB::getQueryLog();
		$this->layout->content = View::make('frontend.home');
	}

	public function getHome() {
		$this->getIndex();
	}

	// public function getLogin() {
	// 	if(Sentry::check()) {
 //            if(Sentry::getUser()->hasAccess('customer')) {
 //                return Redirect::action('UserProductController@getIndex');
 //            } else {
	// 			$this->layout->content = View::make('frontend.login');
 //            }
 //        } else {
	// 		$this->layout->content = View::make('frontend.login');
 //        }
	// }

	// public function postLogin() {
	// 	try {
 //    		// Login credentials
	// 		$credentials = array(
	// 			'email'    => Input::get('email'),
	// 			'password' => Input::get('password'),
	// 			);
 //    		// Authenticate the user
	// 		$user = Sentry::authenticate($credentials, false);
 //            return Redirect::intended('home');
	// 	} catch (Cartalyst\Sentry\Users\LoginRequiredException $e) {
 //            return Redirect::action('AdminUserController@getLogin')
 //                ->with('error', 'Login field is required.')->withInput();;
 //        } catch (Cartalyst\Sentry\Users\PasswordRequiredException $e) {
 //            return Redirect::action('AdminUserController@getLogin')
 //                ->with('error', 'Password field is required.')->withInput();;
 //        } catch (Cartalyst\Sentry\Users\WrongPasswordException $e) {
 //            return Redirect::action('AdminUserController@getLogin')
 //                ->with('error', 'Wrong password, try again.')->withInput();;
 //        } catch (Cartalyst\Sentry\Users\UserNotFoundException $e) {
 //            return Redirect::action('AdminUserController@getLogin')
 //                ->with('error', 'User was not found.')->withInput();;
 //        } catch (Cartalyst\Sentry\Users\UserNotActivatedException $e) {
 //            return Redirect::action('AdminUserController@getLogin')
 //                ->with('error', 'User is not activated.')->withInput();;
 //        }
	// }

	// public function getRegister() {
	// 	if(Sentry::check()) {
 //            $user = Sentry::findUserByID(Sentry::getUser()->id);
 //            if($user->hasAccess('customer')) {
 //                return Redirect::action('UserProductController@getIndex');
 //            } else {
	// 		$this->layout->content = View::make('frontend.register');
 //            }
 //        } else {
	// 		$this->layout->content = View::make('frontend.register');
 //        }
	// }

	// public function postRegister() {
	// 	$validator = Validator::make(Input::all(), Customer::$rules);
 //        if ($validator->passes()) {
 //        	// Create the user
 //        	$user = Sentry::createUser(array(
 //        		'name'      => Input::get('name'),
 //        		'email'     => Input::get('email'),
 //        		'password'  => Input::get('password'),
 //        		'activated' => true,
 //        		));
 //            // Find the group using the group id
 //        	$administratorGroup = Sentry::findGroupByName('Customer');
 //            // Assign the group to the user
 //        	$user->addGroup($administratorGroup);
 //            return Redirect::action('UserHomeController@getLogin')
 //            	->with('success', 'Customer successfully added! Now you can login with your account.');
 //        } else {
 //            return Redirect::action('UserHomeController@getRegister')
 //            	->with('error', 'The following errors occurred')
 //            	->withErrors($validator)
 //            	->withInput();
 //        }
	// }

	// public function getLogout() {
	// 	Sentry::logout();
	// 	return Redirect::action('UserHomeController@getLogin')
	// 		->with('error', 'Your are now logged out!');
	// }

}
