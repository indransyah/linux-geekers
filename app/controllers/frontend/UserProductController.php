<?php

class UserProductController extends BaseController {

	protected $layout = 'frontend.layouts.master';

	public function getIndex()
	{
		// $products = Product::with(array('picture' => function($query) {
		// 	$query->first();
		// }))->get();
		// $products = Product::with('picture')->get();
		$products = Product::published()->get();
		// return $products[1]->picture->first()->name;
		// $products = Product::with('picture')->get();
		// return $products[0]->picture->first()->name;
		// return 'Hi Admin on User';
		// $picture = Picture::where('product_id', 1)->count();
		// return $products[0]->picture->count();
		$this->layout->content = View::make('frontend.product.index')
			->with('products', $products);
	}

	public function getPreOrder() {
		$products = Product::published()->where('system', 'pre order')->get();
		$this->layout->content = View::make('frontend.product.index')
			->with('products', $products);
	}

	public function getReadyStock() {
		$products = Product::published()->where('system', 'ready stock')->get();
		$this->layout->content = View::make('frontend.product.index')
			->with('products', $products);
	}

	public function getShow($slug) {
		$product = Product::published()->where('slug', $slug)->with('picture')->first();
		// return $product;
		$this->layout->content = View::make('frontend.product.show')
			->with('product', $product);
	}

	public function getSearch() {

	}

	public function postSearch() {
		$products = Product::published()->where('name', 'like', '%'.Input::get('keyword').'%')->with(array('picture' => function($query) {
			$query->first();
		}))->get();
		// return $products->count();
		// $products = Product::with('picture')->get();
		// return $products[0]->picture->first()->name;
		// return 'Hi Admin on User';
		$this->layout->content = View::make('frontend.product.index')
			->with('products', $products);
	}

}
