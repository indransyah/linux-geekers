<?php

class AdminPreOrderController extends BaseController {

	protected $layout = 'backend.layouts.master';

	public function getIndex() {
		$products = Product::withOrder();
		$this->layout->content = View::make('backend.preorder.index')
			->with('products', $products);
	}

	public function getShow($id) {
		$product = Product::find($id);
		// return $product->withOrderDetail();
		$this->layout->content = View::make('backend.preorder.show')
			->with('product', $product);

	}
}
