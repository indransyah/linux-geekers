<?php

class AdminHomeController extends BaseController {

	protected $layout = 'backend.layouts.master';

	public function getIndex() {
		$this->layout->content = View::make('backend.home');
	}

}
