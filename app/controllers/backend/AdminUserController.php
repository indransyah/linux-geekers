<?php

class AdminUserController extends BaseController {

	protected $layout = 'backend.layouts.master';

    public function __construct() {
        $this->beforeFilter('administrator', array('except' => array('getLogin', 'postLogin', 'getLogout')));
    }

	public function getIndex() {
		$group = Sentry::findGroupByName('Administrator');
    $users = Sentry::findAllUsersInGroup($group);
    $this->layout->content = View::make('backend.user.index')
			->with('users', $users);
	}

	public function getCreate() {
    	$this->layout->content = View::make('backend.user.create');
	}

	public function postStore() {
		$validator = Validator::make(Input::all(), User::$rules);
        if ($validator->passes()) {
            // Create the user
            $user = Sentry::createUser(array(
                'name'      => Input::get('name'),
                'email'     => Input::get('email'),
                'password'  => Input::get('password'),
                'activated' => true,
                ));
            // Find the group using the group id
            $administratorGroup = Sentry::findGroupByName('Administrator');
            // Assign the group to the user
            $user->addGroup($administratorGroup);
            return Redirect::action('AdminUserController@getIndex')
            	->with('success', 'User successfully added!');
        } else {
            return Redirect::action('AdminUserController@getCreate')
            	->with('error', 'The following errors occurred')
            	->withErrors($validator)
            	->withInput();
        }
	}

	public function deleteDestroy($id) {
        // Find the user using the user id
        $user = Sentry::findUserById($id);
        // Delete the user
        $user->delete();
        if($id == Sentry::getUser()->id) {
            $this->getLogout();
        }
        return Redirect::action('AdminUserController@getIndex')
            ->with('success', 'User successfully deleted!');
	}

	public function getProfile() {
    	$this->layout->content = View::make('backend.profile');
    }

    public function postProfile() {
    	$input = Input::all();
    	$rules = User::$rules;
    	$rules['email'] .= ',' . Sentry::getUser()->id . ',id';
    	unset($rules['password']);
    	unset($rules['password_confirmation']);
    	$validator = Validator::make($input, $rules);
    	if ($validator->passes()) {
    		$user = Sentry::findUserById(Sentry::getUser()->id);
    		$user->name = Input::get('name');
    		$user->email = Input::get('email');
    		$user->save();
    		return Redirect::action('AdminUserController@getProfile')
    		  ->with('success', 'Profile successfully changed!');
    	} else {
    		return Redirect::action('AdminUserController@getProfile')
    		  ->with('error', 'The following errors occurred')
    		  ->withErrors($validator)
    		  ->withInput();
    	}
    }

    public function getPassword() {
    	$this->layout->content = View::make('backend.password');
    }

    public function postPassword() {
        $user = Sentry::findUserById(Sentry::getUser()->id);
    	$check = $user->checkPassword(Input::get('current_password'));
        if ($check) {
            $input = Input::all();
            $rules = User::$rules;
            unset($rules['name']);
            unset($rules['email']);
            $validator = Validator::make($input, $rules);
            if ($validator->passes()) {
                $user->password = Input::get('password');
                $user->save();
                return Redirect::action('AdminUserController@getPassword')
                    ->with('success', 'Password successfully changed!');
            } else {
                return Redirect::action('AdminUserController@getPassword')
                    ->with('error', 'The following errors occurred')
                    ->withErrors($validator)
                    ->withInput();
            }
        } else {
            return Redirect::action('AdminUserController@getPassword')
                ->with('error', 'Wrong current password!');
        }
    }

	public function getLogin() {
        if(Sentry::check()) {
            if(Sentry::getUser()->hasAccess('administrator')) {
                return Redirect::action('AdminHomeController@getIndex')->with('success', 'You are already logged in!');
            } else {
                return View::make('backend.login');
            }
        } else {
            return View::make('backend.login');
        }
	}

	public function postLogin() {
        try {
            // Login credentials
            $credentials = array(
                'email'    => Input::get('email'),
                'password' => Input::get('password'),
            );

            // Authenticate the user
            $user = Sentry::authenticate($credentials, false);
            return Redirect::intended(URL::action('AdminHomeController@getIndex'))
                ->with('success', 'You are now logged in!');

        } catch (Cartalyst\Sentry\Users\LoginRequiredException $e) {
            return Redirect::action('AdminUserController@getLogin')
                ->with('error', 'Login field is required.')->withInput();;
        } catch (Cartalyst\Sentry\Users\PasswordRequiredException $e) {
            return Redirect::action('AdminUserController@getLogin')
                ->with('error', 'Password field is required.')->withInput();;
        } catch (Cartalyst\Sentry\Users\WrongPasswordException $e) {
            return Redirect::action('AdminUserController@getLogin')
                ->with('error', 'Wrong password, try again.')->withInput();;
        } catch (Cartalyst\Sentry\Users\UserNotFoundException $e) {
            return Redirect::action('AdminUserController@getLogin')
                ->with('error', 'User was not found.')->withInput();;
        } catch (Cartalyst\Sentry\Users\UserNotActivatedException $e) {
            return Redirect::action('AdminUserController@getLogin')
                ->with('error', 'User is not activated.')->withInput();;
        }
	}

	public function getLogout() {
        Sentry::logout();
        return Redirect::action('AdminUserController@getLogin')
            ->with('error', 'Your are now logged out!');
    }

}
