<?php

class AdminSettingController extends BaseController {

	protected $layout = 'backend.layouts.master';

	public function getIndex() {
		$this->getCompany();
	}

	public function postCoba($id) {
		return $id;
	}

	public function getCompany() {
		$company = Company::first();
		// $company->logo = STAPLER_NULL;
		// $company->save();
		// echo "<pre>";
		// var_dump($company->logo->url());
		// echo "</pre>";
		// return 'ok';
		$this->layout->content = View::make('backend.setting.company')
			->with('company', $company);
	}

	public function postCompany() {
		$validator = Validator::make(Input::all(), Company::$rules);
		if ($validator->passes()) {
			$company = Company::find(1);
			$company->name = Input::get('name');
			$company->description = Input::get('description');
			$company->address = Input::get('address');
			$company->phone = Input::get('phone');
			$company->email = Input::get('email');
			$company->bbm = Input::get('bbm');
			$company->facebook = Input::get('facebook');
			$company->twitter = Input::get('twitter');
			$company->logo = Input::file('logo');
			$company->save();
			return Redirect::action('AdminSettingController@getCompany')
				->with('success', 'Company profile was successfully change');
		} else {
			return Redirect::action('AdminSettingController@getCompany')
				->with('error', 'The following errors occurred')
				->withErrors($validator)
				->withInput();
		}
	}

	public function getHomepage() {
		// return Carousel::take(5)->get()->count();
		// if (Carousel::take(5)->get()->count() != 0) {
		// 	return 'ok';
		// } else {
		// 	return 'nope';
		// }
		$images = Carousel::all();
		$this->layout->content = View::make('backend.setting.homepage')->with('images', $images);
	}

	public function postImage() {
		$files = Input::file('files');
		foreach ($files as $file) {
			$carousel = new Carousel();
			$carousel->carousel = $file;
			$carousel->save();
			$name = $carousel->carousel->url('medium');
			$id = $carousel->id;
			$results[] = compact('name');
			// $id[] = compact('id');
		}
		return array(
			'files' => $results,
			'id' => $id
			);

	}

	public function getDelete($id) {
		$image = Carousel::find($id);
		$image->delete();
		return Redirect::action('AdminSettingController@getHomepage')->with('success', 'Successfully delete the image.');
	}

	public function postSidebar($id) {
		// return $id;
		$sidebar = Sidebar::find($id);
		$sidebar->content = Input::get('content');
		$sidebar->save();
		return Redirect::action('AdminSettingController@getHomepage')->with('success', 'Change was successfully saved');
	}

}
