<?php

class AdminShipmentController extends BaseController {

	protected $layout = 'backend.layouts.master';

	public function getIndex() {
		// $shipments = Shipment::has('invoice')->get();
		// $shipments = Shipment::whereHas('invoice', function($q) {
		// 	$q->whereHas('payment', function($q) {
		// 		$q->where('status', 'lunas');
		// 	});
		// })->get();
		// return $shipments->invoice;
		// $shipments = Shipment::whereHas('order', function($q) {
		// 		$q->where('status', '=', 'telah dikonfirm');
		// 	})->get();
		$shipments = Shipment::whereHas('order', function ($q) {
			$q->whereHas('invoice', function ($q) {
				$q->whereHas('payment', function($q) {
					$q->where('status', 'lunas');
				});
			});
		})->orderBy('created_at', 'desc')->get();
		$packaging = Shipment::where('status', 'packaging')->get();
		// $shipments = Shipment::with('order.invoice')->get();
		// return $shipments[0]->recipient->name;
        // return count($shipments);

		$this->layout->content = View::make('backend.shipment.index')
			->with('shipments', $shipments)
			->with('packaging', $packaging);
	}

	public function getShow($id) {
		$shipment = Shipment::find($id);
		$this->layout->content = View::make('backend.shipment.show')
			->with('shipment', $shipment);
	}

	public function postUpdate($id) {
		$shipment = Shipment::find($id);
		$shipment->tracking_number = Input::get('tracking-number');
		$shipment->date = date("Y-m-d H:i:s");
		$shipment->status = Input::get('status');
		$shipment->save();
		return Redirect::action('AdminShipmentController@getShow', $shipment->id);
	}

	public function getPrint($id) {
		$recipient = Recipient::find($id);
		$pdf = PDF::loadView('backend.pdf.recipient', compact('recipient'));
		return $pdf->stream();
	}

	public function getBulkPrint() {
		$recipients = Recipient::whereHas('shipment', function($q) {
			$q->where('status', 'packaging');
		})->get();
		// return $recipients;
		$pdf = PDF::loadView('backend.pdf.recipients', compact('recipients'));
		return $pdf->stream();
	}

}
