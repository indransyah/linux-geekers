<?php

class AdminCustomerController extends BaseController {

	protected $layout = 'backend.layouts.master';

	public function getIndex() {
		$group = Sentry::findGroupByName('Customer');
    $customers = Sentry::findAllUsersInGroup($group);
    $this->layout->content = View::make('backend.customer.index')
			->with('customers', $customers);
	}

	public function getShow($id)
	{
		$customer = User::find($id);
		$this->layout->content = View::make('backend.customer.show')
			->with('customer', $customer);
	}

}
