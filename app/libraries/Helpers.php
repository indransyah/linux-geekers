<?php

class Helpers {

	public static function rupiah($int) {
		return 'Rp '.number_format($int, 2, ',', '.');
	}

	public static function date($date) {
		return date("d F Y",strtotime($date)).' at '.date("G:i",strtotime($date));
	}

	public static function checkCart() {
		// Check the shopping cart, apakah produk yang ada pada cart masih ada pada db?

		$cart = Cart::content();
		foreach ($cart as $row) {
			$product = Product::find($row->id);
			if (!$product) {
				Cart::remove($row->rowid);
			}
		}
	}

	public static function sendEmail() {
		$data = ['title' => 'Selamat datang di Linux Geekers'];
		Mail::send('backend.email.invoice', $data, function($m){
			$m->to('indransyah@gmail.com', 'Indra');
			$m->subject('Invoice');
		});
	}

}
