<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddCarouselFieldsToCarouselTable extends Migration {

    /**
     * Make changes to the table.
     *
     * @return void
     */
    public function up()
    {   
        Schema::table('carousel', function(Blueprint $table) {     
            
            $table->string('carousel_file_name')->nullable();
            $table->integer('carousel_file_size')->nullable();
            $table->string('carousel_content_type')->nullable();
            $table->timestamp('carousel_updated_at')->nullable();

        });

    }

    /**
     * Revert the changes to the table.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('carousel', function(Blueprint $table) {

            $table->dropColumn('carousel_file_name');
            $table->dropColumn('carousel_file_size');
            $table->dropColumn('carousel_content_type');
            $table->dropColumn('carousel_updated_at');

        });
    }

}
