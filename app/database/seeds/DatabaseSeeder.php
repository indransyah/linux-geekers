<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('GroupTableSeeder');
		$this->call('UserTableSeeder');
		$this->call('PageTableSeeder');
		$this->call('CompanyTableSeeder');
		$this->call('SidebarTableSeeder');
		$this->call('SizeTableSeeder');
	}

}
