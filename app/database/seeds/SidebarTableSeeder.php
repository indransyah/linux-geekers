<?php

class SidebarTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('sidebar')->delete();
		Sidebar::create(array(
			'name' => 'Left 1',
			'content' => ''
		));

		Sidebar::create(array(
			'name' => 'Left 2',
			'content' => ''
		));

		Sidebar::create(array(
			'name' => 'Right 1',
			'content' => ''
		));

		Sidebar::create(array(
			'name' => 'Right 2',
			'content' => ''
		));
	}

}
