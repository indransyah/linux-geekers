<?php

class PageTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('pages')->delete();
		Page::create(array(
			'title' => 'About',
			'slug' => 'about',
			'content' => '<b>About page</b>',
			'user_id' => 1
		));
		Page::create(array(
			'title' => 'Contact',
			'slug' => 'contact-us',
			'content' => '<b>Contact page</b>',
			'user_id' => 1
		));
		Page::create(array(
			'title' => 'How to',
			'slug' => 'how-to',
			'content' => '<b>How to order page</b>',
			'user_id' => 1
		));
		Page::create(array(
			'title' => 'TOS',
			'slug' => 'tos',
			'content' => '<b>TOS page</b>',
			'user_id' => 1
		));

	}

}
