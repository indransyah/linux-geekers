<?php

class GroupTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('groups')->delete();
		
		// Create the group
		$group = Sentry::createGroup(array(
			'name'        => 'Administrator',
			'permissions' => array(
				'customer'	=> 1,
				'administrator' => 1,
				),
			));

		$group = Sentry::createGroup(array(
			'name'        => 'Customer',
			'permissions' => array(
				'customer' => 1,
				),
			));

	}

}
