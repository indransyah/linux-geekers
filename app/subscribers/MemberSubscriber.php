<?php

class MemberSubscriber {
	public function subscribe($events) {
		$events->listen('reset.password', 'MemberSubscriber@onResetPassword');
	}

	public function onResetPassword($email, $resetCode) {
		Mail::send('frontend.email.reset', array('resetCode' => $resetCode), function($message) use ($email) {
			$message->to($email, $email)->subject('Reset Password');
		});
	}
}