<?php

class Size extends Eloquent {

	protected $table = 'sizes';

    public function product() {
        return $this->belongsToMany('Product', 'product_size', 'size_id', 'product_id')->withPivot('quota');
    }

    public function scopefindIdFromValue($query, $value) {
    	return $query->where('value', $value)->first()->id;
    }

}