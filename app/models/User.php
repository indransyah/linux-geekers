<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');
	// protected $primaryKey = 'user_id';
	public static $rules = array(
		'name' => 'required|max:50',
		// 'username' =>'required|max:15|unique:users,username',
		'email' =>'required|email|max:30|unique:users,email',
		'password' =>'required|alpha_num|between:6,12|confirmed',
		'password_confirmation' =>'required|alpha_num|between:6,12'
	);

	public function page() {
		return $this->hasMany('Page');
	}

	public function recipient() {
		return $this->hasMany('Recipient');
	}

	public function order() {
		return $this->hasMany('Order');
	}

	// public function invoice() {
	// 	return $this->hasMany('Invoices');
	// }

}
