<?php

class Page extends Eloquent {

	protected $table = 'pages';
	// protected $primaryKey = 'page_id';
	public static $rules = array(
		'title' => 'required|max:100|unique:pages,title',
		'content' => 'required'
	);

	public function user() {
		return $this->belongsTo('User');
	}

	public function scopeMenu($query) {
		return $query->select('title', 'slug')->where('featured', 1)->get();
	}

	public function scopeSidebar($query) {
		return $query->select('title', 'slug')->get();
	}

}