<?php

class Order extends Eloquent {

	protected $table = 'orders';
    protected $dates = ['date'];

    public function shipment() {
        return $this->hasOne('Shipment');
    }

    public function invoice() {
        return $this->hasOne('Invoice');
    }

    public function product() {
        return $this->belongsToMany('Product', 'order_detail', 'order_id', 'product_id')->withPivot('qty', 'size', 'price', 'subtotal');
    }

    public function user() {
        return $this->belongsTo('User');
    }

    public function scopeUnconfirmed() {
        return $this->where('status', 'belum dikonfirmasi');
    }



}