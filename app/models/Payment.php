<?php

class Payment extends Eloquent {

	protected $table = 'payments';

	public function invoice() {
        return $this->belongsTo('Invoice');
    }

    public function scopeUnconfirmed() {
        return $this->where('status', 'proses')->where('amount', '!=', 0);
    }
}