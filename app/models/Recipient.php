<?php

class Recipient extends Eloquent {

	protected $table = 'recipients';

	public function user() {
		return $this->belongsTo('User');
	}

	public function shipment() {
        return $this->hasMany('Shipment');
    }

}