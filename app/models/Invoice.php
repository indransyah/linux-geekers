<?php

class Invoice extends Eloquent {

	protected $table = 'invoices';

    public function order() {
        return $this->belongsTo('Order');
    }

    // public function user() {
    //     return $this->belongsTo('User');
    // }

    public function payment() {
        return $this->hasOne('Payment');
    }

}