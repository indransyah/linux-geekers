<?php

class Shipment extends Eloquent {

	protected $table = 'shipments';

	public function recipient() {
        return $this->belongsTo('Recipient');
    }

    public function order() {
        return $this->belongsTo('Order');
    }

}