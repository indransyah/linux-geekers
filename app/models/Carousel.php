<?php

use Codesleeve\Stapler\ORM\StaplerableInterface;
use Codesleeve\Stapler\ORM\EloquentTrait;

class Carousel extends Eloquent implements StaplerableInterface {
    use EloquentTrait;

    protected $table = 'carousel';

    public function __construct(array $attributes = array()) {
        $this->hasAttachedFile('carousel', [
            'styles' => [
                'medium' => '300x300',
            ],
            'url' => '/images/:attachment/:id_partition/:style/:filename',
            'default_url' => '/images/default-carousel.jpg'
        ]);

        parent::__construct($attributes);
    }
}